import java.lang.Exception
import java.util.*


class Game{
    private var x: Player = Player('x')
    private var o: Player = Player('o')
    private var table = Board(x,o)
    var countTurn: Int = 0
    val kb = Scanner(System.`in`)
    var c = 0


    fun printTable(Table: Array<CharArray>) {
        println("0 1 2")
        for (row in Table) {
        for (col in row) {
            print(col+ " ")
        }
        println( c)
            c+=1
    }
}
    private fun newGame() {
        table = Board(x,o)
    }

    private fun inputRowCol() {

        while(true) {
            try {
                println(table.getCurrentPlayer().getName()+" Turn")
                println("Please input row(0-2) col(0-2) : ")

                val row = kb.nextInt()
                val col = kb.nextInt()
                val index: Int = (row * 3) + col

                if(row< 0 || row>2){
                    println("value of row must between 0-2.")
                    continue
                }
                if(col< 0 || col>2){
                    println("value of col must between 0-2.")
                    continue
                }
                if(table.checkEmpty(row,col)){
                    println("This position was chosen please change.")
                    continue
                }

                table.setBoard(row,col, table.getCurrentPlayer().getName())
                countTurn =+1
                break
            }catch (e: Exception){
                println("Position Incorrect")
                if (kb.hasNext()) {
                    kb.nextLine()
                }

            }
        }
    }

    private fun play () {
        while (true) {
            printTable(table.getTable())
            inputRowCol()
            if(this.table.isEnd()) {
                printTable(table.getTable())
                printWinner()
                //print("3")
                break
            }

        }
    }
    fun start(){
        while(true) {
            newGame()
            play()
            if(!replay()) break
        }

    }

    private fun replay(): Boolean {
        while (true) {
            var ans: String
            print("Play Again  (y/n)")
            ans = kb.next()
            if (ans == "y" ) return true
            if (ans == "n") return false
        }
    }

    private fun printWinner() {
        if(this.table.getWinner() == null) {
            println("Draw")
        }else {
            println("Winner is "+this.table.getWinner()!!.getName()+".")
        }

    }

}