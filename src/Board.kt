class Board (private var x: Player,private var o: Player) {

    var table1 = arrayOf(charArrayOf('-','-','-'),
        charArrayOf('-','-','-'),
        charArrayOf('-','-','-'))

    private var winner: Player? = null
    private var turn = 0

    fun getTable () = this.table1

    fun getWinner() = this.winner


    fun setBoard(row: Int,col: Int, item: Char) {
        this.table1[row][col] = item
    }

    fun getCurrentPlayer (): Player {
        if (this.turn % 2 == 0) return this.x
        return this.o
    }

    private fun changeTurn() { turn++ }


    fun isEnd():Boolean {
        if(checkWin()) {
            setWinner()
            return true
        }
        if (checkDraw()){
            return true
        }
        this.changeTurn()
        return false

    }

    private fun checkCol(): Boolean {
        if (this.table1[0][0] == this.table1[1][0] && this.table1[1][0] == this.table1[2][0] && this.table1[0][0] != '-') return true
        if (this.table1[0][1] == this.table1[1][1] && this.table1[1][1] == this.table1[2][1] && this.table1[0][1] != '-') return true
        if (this.table1[0][2] == this.table1[1][2] && this.table1[1][2] == this.table1[2][2] && this.table1[0][2] != '-') return true
        return false
    }
    private fun checkRow():Boolean {
        if (this.table1[0][0] == this.table1[0][1] && this.table1[0][1] == this.table1[0][2] && this.table1[0][0] != '-')  return true
        if (this.table1[1][0] == this.table1[1][1] && this.table1[1][1] == this.table1[1][2] && this.table1[1][0] != '-') return true
        if (this.table1[2][0] == this.table1[2][1] && this.table1[2][1] == this.table1[2][2] && this.table1[2][0] != '-') return true
        return false
    }
    private fun checkDiagonal(): Boolean {
        if (this.table1[0][0] == this.table1[1][1] && this.table1[1][1] == this.table1[2][2]&& this.table1[0][0] != '-') return true
        if (this.table1[0][2] == this.table1[1][1] && this.table1[1][1] == this.table1[2][0]&& this.table1[0][2] != '-') return true
        return false;
    }

    private fun checkWin(): Boolean {
        if(checkRow()) return true
        if(checkCol()) return true
        if(checkDiagonal()) return true
        return false
    }

    private fun checkDraw(): Boolean {
        if( turn == 8) return true
        return false
    }

    fun checkEmpty(row:Int ,col:Int): Boolean{
        if(this.table1[row][col] == '-') return false
        return true
    }

    fun setWinner() {
        this.winner = this.getCurrentPlayer()
    }



}